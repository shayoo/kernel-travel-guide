<?php
	require_once 'include/config.inc.php';
	require_once 'include/__functions.php';	?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="<?php echo getInfo("site_contents","meta_des","WHERE `content_id`=1");?>">
<meta name="keywords" content="<?php echo getInfo("site_contents","meta_keywords","WHERE `content_id`=1");?>">
<title><?php echo Site_Title?></title>
<link href="global.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" type="text/css" href="engine1/style.css" />
<script type="text/javascript" src="engine1/jquery.js"></script>
</head>

<body>
<div id="wrapper">
  <?php require_once 'header.php'; ?>
  <div id="wowslider-container1">
    <div class="mb1"></div>
    <div class="ws_images">
      <ul>
        <li><img src="data1/images/1.jpg" alt="1" title="1" id="wows1_0"/></li>
        <li><img src="data1/images/2.jpg" alt="2" title="2" id="wows1_1"/></li>
      </ul>
    </div>
    <div class="ws_bullets">
      <div> <a href="#" title="1"><img src="data1/tooltips/1.jpg" alt="1"/>1</a> <a href="#" title="2"><img src="data1/tooltips/2.jpg" alt="2"/>2</a> </div>
    </div>
  </div>
  <script type="text/javascript" src="engine1/wowslider.js"></script> 
  <script type="text/javascript" src="engine1/script.js"></script>
  <div class="main-containor">
    <div class="left_x1">
    
      <h1><?php echo getinfo('site_contents','title1',"WHERE `content_id`=1"); ?></h1>
      <?php echo getinfo('site_contents','des',"WHERE `content_id`=1"); ?>
      <div>
      	
        <?php $deals_query = mysql_fetch_assoc(mysql_query("SELECT * FROM `deals` WHERE `id`=1")); 
			  $short_desc = utf8_decode($deals_query['short_des']);
		?>
        <div class="controller-bn-1 margin-right-5">
          <h1><?php echo $deals_query['title']; ?></h1>
          <div class="thumb"><img src="<?php echo $deals_query['img1']; ?>" width="218" height="122" alt="" /></div>
          <div class="desc">
          <p><?php echo strtr($short_desc,"?","'"); ?></p>
          </div>
          <a href="#" class="read">Read More</a> </div>
        
		
		<?php $deals_query = mysql_fetch_assoc(mysql_query("SELECT * FROM `deals` WHERE `id`=2")); 
			  $short_desc = utf8_decode($deals_query['short_des']);
		?>
        <div class="controller-bn-1 margin-right-5">
          <h1><?php echo $deals_query['title']; ?></h1>
          <div class="thumb"><img src="<?php echo $deals_query['img1']; ?>" width="218" height="122" alt="" /></div>
          <div class="desc">
          <p><?php echo strtr($short_desc,"?","'"); ?></p>
          </div>
          <a href="#" class="read">Read More</a> </div>
          
         <?php $deals_query = mysql_fetch_assoc(mysql_query("SELECT * FROM `deals` WHERE `id`=3")); 
		 	   $short_desc = utf8_decode($deals_query['short_des']);
		 ?> 
        <div class="controller-bn-1 ">
          <h1><?php echo $deals_query['title']; ?></h1>
          <div class="thumb"><img src="<?php echo $deals_query['img1']; ?>" width="218" height="122" alt="" /></div>
          <div class="desc">
            
            <p><?php echo strtr($short_desc,"?","'"); ?></p>
          </div>
          <a href="#" class="read">Read More</a> </div>
        
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="left_x2">
      <div class="email">
        <h1>User login</h1>
        <input name="" type="text" class="txt" />
        <input name="" type="text" class="txt" />
        <div>
          <div style="float:left; position:relative; left:5px; padding-bottom:10px;">
            <input name="" type="image" src="images/register.png" />
          </div>
          <div style="float:right; position:relative; right:5px; padding-bottom:10px;">
            <input name="" type="image" src="images/login.png" />
          </div>
          <div class="clear"></div>
        </div>
        <div style="padding:5px 5px 15px; text-align:right;"><a href="#" class="link">Forgot your password?</a></div>
      </div>
      <div class="email">
        <h1>Industry</h1>
        <ul class="mmj">
          <li><a href="#">+  Accountancy & finance jobs</a></li>
          <li><a href="#">+  Education & training jobs</a></li>
          <li><a href="#">+  Fundraising & charity jobs</a></li>
          <li><a href="#">+  HR jobs</a></li>
          <li><a href="#">+  Marketing jobs</a></li>
          <li><a href="#">+  Policy & strategy jobs</a></li>
          <li><a href="#">+  Property & construction jobs</a></li>
          <li><a href="#">+  Public sector jobs</a></li>
          <li><a href="#">+  Transportation & energy jobs</a></li>
        </ul>
      </div>
      <div class="email">
        <h1>Upcoming Events</h1>
        <p><strong style="font-size:16px; color:#999;">Date: May 14 - 15, 2013</strong><br />
          <span style="font-size:14px; color:#4b4b4b;">Westin San Francisco Market Street - San Francisco, CA</span></p>
        <div style="padding:5px 5px 15px; text-align:right;"><a href="#" class="link">Event Details</a></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
      <?php require_once 'footer.php'; ?>
    
    <div class="clear"></div>
  </div>
  <div class="clear"></div>
</div>
<div class="ft"> © Copyright 2013 - Rumi  Associates. All Rights Reserved. </div>
<div class="footer"></div>
</body>
</html>
