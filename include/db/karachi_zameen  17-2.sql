-- phpMyAdmin SQL Dump
-- version 2.11.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 18, 2013 at 02:30 AM
-- Server version: 5.0.45
-- PHP Version: 5.2.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `karachi_zameen`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL auto_increment,
  `admin_name` varchar(50) default NULL,
  `admin_password` varchar(50) default NULL,
  `admin_email` varchar(50) default NULL,
  `admin_last_login` varchar(255) default NULL,
  `admin_ip` varchar(255) default NULL,
  `status` varchar(3) default NULL,
  `pdate` date default NULL,
  PRIMARY KEY  (`admin_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_name`, `admin_password`, `admin_email`, `admin_last_login`, `admin_ip`, `status`, `pdate`) VALUES
(1, 'admin', 'admin', 'shaukatkhan@hotmail.com', '30 January 2013  7:04PM', '127.0.0.1', 'Yes', '2013-01-30'),
(2, 'shaukat', 'admin', 'shaukat.khan188@yahoo.com', '30 January 2013  6:43PM', '127.0.0.1', 'Yes', '2013-01-30');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `cat_id` int(11) NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  `des` text,
  `img1` varchar(99) default NULL,
  `s_img1` varchar(255) default NULL,
  `meta_title` varchar(250) default NULL,
  `meta_des` text,
  `meta_keywords` text,
  `status` varchar(3) default NULL,
  PRIMARY KEY  (`cat_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cat_id`, `title`, `des`, `img1`, `s_img1`, `meta_title`, `meta_des`, `meta_keywords`, `status`) VALUES
(15, 'Other Cities Properties', 'Suspendisse ullamcorper quam et enim convallis ac mattis est bibendum.', 'img_category/larg_15.png', 'img_category/thumb_15.png', '', NULL, NULL, 'Yes'),
(13, ' D.H.A Properties', 'Suspendisse ullamcorper quam et enim convallis ac mattis est bibendum.', 'img_category/larg_13.png', 'img_category/thumb_13.png', '', NULL, NULL, 'Yes'),
(14, 'Port Qasim Properties', 'Suspendisse ullamcorper quam et enim convallis ac mattis est bibendum.', 'img_category/larg_14.png', 'img_category/thumb_14.png', '', NULL, NULL, 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `header_img`
--

CREATE TABLE `header_img` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(99) collate latin1_general_ci default NULL,
  `img1` varchar(99) collate latin1_general_ci default NULL,
  `s_img1` varchar(99) collate latin1_general_ci default NULL,
  `status` varchar(99) collate latin1_general_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `header_img`
--

INSERT INTO `header_img` (`id`, `title`, `img1`, `s_img1`, `status`) VALUES
(1, '1st header image title', 'header_img/larg_1.jpg', 'header_img/thumb_1.jpg', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `memberId` int(11) NOT NULL auto_increment,
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(50) NOT NULL default '',
  `state` varchar(50) default NULL,
  `country` varchar(50) NOT NULL default '',
  `zipcode` varchar(50) NOT NULL default '0',
  `phone` varchar(50) default NULL,
  `email` varchar(50) default NULL,
  `password` varchar(50) default NULL,
  `sub_date` varchar(50) default NULL,
  `status` varchar(10) default NULL,
  `activ` varchar(11) NOT NULL default '0',
  PRIMARY KEY  (`memberId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`memberId`, `firstName`, `lastName`, `address`, `city`, `state`, `country`, `zipcode`, `phone`, `email`, `password`, `sub_date`, `status`, `activ`) VALUES
(1, 'John', 'Adam', 'House # 444', 'UK', 'UK', '', '75000', '02527524524', 'khalique.ahmed3@hotmail.com', '123', NULL, 'Yes', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(255) NOT NULL auto_increment,
  `title` varchar(99) collate latin1_general_ci default NULL,
  `des` varchar(999) collate latin1_general_ci default NULL,
  `_date` varchar(99) collate latin1_general_ci default NULL,
  `meta_title` varchar(250) collate latin1_general_ci default NULL,
  `meta_des` text collate latin1_general_ci,
  `meta_keywords` text collate latin1_general_ci,
  `status` varchar(99) collate latin1_general_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `des`, `_date`, `meta_title`, `meta_des`, `meta_keywords`, `status`) VALUES
(1, 'For Sale', 'Over 100000 properties for  sale and to rent |  Estate Agents, Private Property Sales, houses,  townhouses, farms, PROPERTY for sale,to  rent in South Africa | Over 100000 properties for sale and to rent |  Estate Agents, Private Property Sales, houses, townhouses, farms, PROPERTY for sale,to  rent in South Africa | Over 100000 properties for  sale and to rent |  Estate Agents, Private Property Sales, houses,  townhouses, farms, ...', '01 Jun 2011', NULL, NULL, NULL, 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `properties_details`
--

CREATE TABLE `properties_details` (
  `property_id` int(20) NOT NULL auto_increment,
  `catid` varchar(50) default NULL,
  `sub_catid` varchar(99) default NULL,
  `title` varchar(255) default NULL,
  `city` varchar(200) default NULL,
  `vancity` varchar(255) default NULL,
  `des` text,
  `price` varchar(99) default NULL,
  `area` varchar(100) default NULL,
  `purpose` varchar(200) default NULL,
  `contact_person` varchar(200) default NULL,
  `phone_no` varchar(100) default NULL,
  `email` varchar(300) default NULL,
  `meta_title` varchar(250) default NULL,
  `meta_des` text,
  `meta_keywords` text,
  `feature_status` varchar(3) default NULL,
  `status` varchar(3) default NULL,
  `thumb_image` varchar(255) default NULL,
  `large_image` varchar(255) default NULL,
  `xlarge_img` varchar(255) default NULL,
  `xxlarge_img` varchar(255) default NULL,
  PRIMARY KEY  (`property_id`),
  FULLTEXT KEY `address` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `properties_details`
--

INSERT INTO `properties_details` (`property_id`, `catid`, `sub_catid`, `title`, `city`, `vancity`, `des`, `price`, `area`, `purpose`, `contact_person`, `phone_no`, `email`, `meta_title`, `meta_des`, `meta_keywords`, `feature_status`, `status`, `thumb_image`, `large_image`, `xlarge_img`, `xxlarge_img`) VALUES
(17, '15', NULL, 'abc', 'rwp', 'perdehi', '<p>\r\n	i have a property same</p>\r\n', '4000000', '400 sq', 'sale', 'ali', '54456556', NULL, '', '', '', 'No', 'Yes', 'img_properties/rct_ENWV17.jpg', 'img_properties/rctlg_ENWV17.jpg', 'img_properties/rctlg_xl_ENWV17.jpg', 'img_properties/orgsize_ENWV17imag32.jpg'),
(14, '13', NULL, 'shaheen benglows', 'karachi', 'DHA phase v', '<p>\r\n	this is our first binglows &gt;..</p>\r\n', '8000000', '500 Squer fit', 'Sale', 'M Asad', '030203344', 'shaukat.khan@gmail.com', 'asd', '', '', 'Yes', 'Yes', 'img_properties/rct_0M6E14.jpg', 'img_properties/rctlg_0M6E14.jpg', 'img_properties/rctlg_xl_0M6E14.jpg', 'img_properties/orgsize_0M6E14imag7.jpg'),
(15, '13', NULL, 'abc', 'hdd', 'DHA phase v', '<p>\r\n	sdsfdsfrdgdg</p>\r\n', '29999', '200 sq', 'sale', 'hshdshh', '7675473547', NULL, '', '', '', 'No', 'Yes', 'img_properties/rct_l14R15.jpg', 'img_properties/rctlg_l14R15.jpg', 'img_properties/rctlg_xl_l14R15.jpg', 'img_properties/orgsize_l14R15imag3.jpg'),
(18, '14', NULL, 'm agency', 'isb', 'f-7', '<p>\r\n	sale this property</p>\r\n', '55555555', '200 sq', 'sale', 'salman', '78878776', 'shaukat.khan@gmail.com', '', '', '', 'Yes', 'Yes', 'img_properties/rct_ewo018.jpg', 'img_properties/rctlg_ewo018.jpg', 'img_properties/rctlg_xl_ewo018.jpg', 'img_properties/orgsize_ewo018imag44.jpg'),
(16, '15', NULL, 'waqar pro', 'khi', 'dha', '<p>\r\n	this is my city property</p>\r\n', '2323343', '232', 'sale', 'salma', '67787878767', 'shaukat.khan@gmail.com', '', '', '', 'Yes', 'Yes', 'img_properties/rct_6H9616.jpg', 'img_properties/rctlg_6H9616.jpg', 'img_properties/rctlg_xl_6H9616.jpg', 'img_properties/orgsize_6H9616imag9.jpg'),
(19, '14', NULL, 'malik riaz', 'isb', 'behria town ', '<p>\r\n	i required this property</p>\r\n', '100000000', '3000 sq', 'purches', 'riaz', '55265362536', NULL, '', '', '', 'Yes', 'Yes', 'img_properties/rct_U1sw19.jpg', 'img_properties/rctlg_U1sw19.jpg', 'img_properties/rctlg_xl_U1sw19.jpg', 'img_properties/orgsize_U1sw19imag8.jpg'),
(20, '13', NULL, 'sons prty', 'karachi', 'gulistani johar plot 23 gali-7 karachi pakistan', '<p>\r\n	this property is my own property, and i sale it .</p>\r\n', '300000000', '300 sq ft', 'sale', 'kashif', '78867776677', 'hshgsha@fgafg.com', '', '', '', 'Yes', 'Yes', 'img_properties/rct_DWd920.jpg', 'img_properties/rctlg_DWd920.jpg', 'img_properties/rctlg_xl_DWd920.jpg', 'img_properties/orgsize_DWd920home12.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `propet_slider`
--

CREATE TABLE `propet_slider` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(50) collate latin1_general_ci NOT NULL,
  `teaser` varchar(500) collate latin1_general_ci default NULL,
  `type` varchar(10) collate latin1_general_ci NOT NULL,
  `url` varchar(500) collate latin1_general_ci NOT NULL,
  `img1` varchar(50) collate latin1_general_ci NOT NULL,
  `status` varchar(50) collate latin1_general_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `propet_slider`
--


-- --------------------------------------------------------

--
-- Table structure for table `pro_more_imgs`
--

CREATE TABLE `pro_more_imgs` (
  `id` int(11) NOT NULL auto_increment,
  `product_id` int(11) NOT NULL,
  `title` varchar(250) default NULL,
  `large_image` varchar(250) default NULL,
  `xlarge_img` varchar(250) default NULL,
  `xxlarge_img` varchar(250) default NULL,
  `thumb_image` varchar(250) default NULL,
  `status` varchar(10) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `pro_more_imgs`
--

INSERT INTO `pro_more_imgs` (`id`, `product_id`, `title`, `large_image`, `xlarge_img`, `xxlarge_img`, `thumb_image`, `status`) VALUES
(1, 1, 'Test', 'pro_more_imgs/rctlg_.jpg', 'pro_more_imgs/rctlg_xl_.jpg', 'pro_more_imgs/orgsize_wv4.jpg', 'pro_more_imgs/rct_.jpg', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `pro_type`
--

CREATE TABLE `pro_type` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(99) collate latin1_general_ci default NULL,
  `status` varchar(99) collate latin1_general_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `pro_type`
--

INSERT INTO `pro_type` (`id`, `title`, `status`) VALUES
(1, 'New Products', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `site_contents`
--

CREATE TABLE `site_contents` (
  `content_id` int(11) NOT NULL auto_increment,
  `pg_title` varchar(255) default NULL,
  `title1` varchar(99) default NULL,
  `des` longtext,
  `meta_title` varchar(250) default NULL,
  `meta_des` text,
  `meta_keywords` text,
  PRIMARY KEY  (`content_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `site_contents`
--

INSERT INTO `site_contents` (`content_id`, `pg_title`, `title1`, `des`, `meta_title`, `meta_des`, `meta_keywords`) VALUES
(1, 'Home', 'Karachi zameen', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut imperdiet dictum semper. Nunc vehicula volutpat odio, nec scelerisque leo tristique sit amet. Nunc commodo, odio at scelerisque rutrum, eros nisi laoreet eros, id pellentesque turpis elit ut massa. Mauris venenatis erat eros. Suspendisse potenti. Cras id nulla libero. Nam rutrum, odio eu ultricies dignissim, sem lorem mollis diam, a dictum mauris leo eget elit. Vivamus volutpat convallis justo id semper.<br />\r\n	<br />\r\n	Quique pulvinar felis non massa mollis varius. Phasellus accumsan dolor id metus feugiat ac semper erat laoreet. Etiam bibendum nisl sed lectus convallis placerat. Nulla facilisi. Mauris purus risus, cursus vitae sagittis vel, laoreet vel leo. Sed rutrum varius malesuada. Suspendisse venenatis urna sit amet risus bibendum adipiscing non eget massa. Sed facilisis risus non elit euismod pellentesque. In justo diam, molestie eu volutpat sit amet, ultricies et eros. Etiam dapibus nibh mauris. Mauris id justo vel dolor faucibus interdum a volutpat quam. Integer quis nunc in mauris consectetur lacinia et et nisl. Quisque non augue diam, sed egestas nunc. Cras non orci in tortor suscipit blandit sed dapibus eros. Pellentesque pharetra enim ac sapien luctus id sodales est dapibus. Nulla mi leo, hendrerit vitae cursus nec, lacinia id urna.<br />\r\n	<br />\r\n	Morbi non erat augue. Proin blandit ante non orci auctor scelerisque ultricies lorem volutpat. Vestibulum id diam elit, vel hendrerit sem. Phasellus id risus elit, nec dignissim magna. Cras bibendum, nulla eu consectetur feugiat, lacus sapien interdum dui, nec congue eros sapien ut dui. Nulla ultrices consectetur sem a venenatis. Ut sapien augue, gravida eu rhoncus sit amet, adipiscing in tortor. Sed pulvinar imperdiet magna, non convallis tortor tincidunt sed. Pellentesque iaculis lacinia tellus, et bibendum nulla rhoncus ut. Proin faucibus nibh eget dui luctus consectetur. Proin sagittis molestie dictum.</p>\r\n', 'property', 'khi zameen', ''),
(2, 'About Us', '', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent adipiscing leo id arcu dictum ut porttitor mi facilisis. Donec malesuada vulputate ullamcorper. Vestibulum volutpat erat a lacus adipiscing pulvinar. Sed odio dui, ultricies at aliquam et, ultrices id lacus.&nbsp;</p>\r\n<p>\r\n	Curabitur nec magna nibh, vitae blandit orci. Maecenas eu ligula ut erat posuere ullamcorper et non risus. Phasellus commodo eros mauris. Nam aliquet, justo facilisis pulvinar elementum, turpis nunc volutpat leo, a vehicula dui odio ut metus. Mauris eu urna neque. Nulla imperdiet felis quis leo pharetra rhoncus consequat massa euismod. Suspendisse semper neque eget arcu tincidunt suscipit sollicitudin tortor rutrum. Nulla imperdiet, sapien quis tempus bibendum, ante leo pharetra dui, a fringilla ligula arcu id orci. Quisque lacinia velit id leo blandit rutrum. Ut convallis tristique est, sed vulputate mauris tristique sed.&nbsp;</p>\r\n<p>\r\n	Ut libero diam, vestibulum mattis vulputate sit amet, egestas id lacus. Fusce sapien lacus, elementum sit amet rutrum ac, pretium quis ipsum. Curabitur eros ligula, posuere nec posuere nec, rutrum condimentum augue. Pellentesque ut ipsum justo.&nbsp;</p>\r\n<p>\r\n	Cras hendrerit velit ac dolor condimentum accumsan. Ut feugiat egestas egestas. Morbi volutpat lacinia velit, sed sagittis purus ultrices quis. Ut ac felis non turpis feugiat mollis. Nullam a nisi nisl. Sed ac nulla urna. Nunc dapibus enim vitae eros aliquam dapibus. Nam commodo iaculis ipsum vitae tempus. Vivamus tincidunt eleifend sagittis.&nbsp;</p>\r\n<p>\r\n	Morbi rhoncus felis et neque viverra sit amet posuere felis aliquam. Vestibulum semper diam vitae arcu vehicula ut commodo dui faucibus.</p>\r\n', '', '', ''),
(3, 'Our Company', '', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempor, felis a commodo porttitor, ante nulla bibendum felis, vitae cursus nisi velit nec quam. In dolor nunc, vulputate non facilisis quis, pretium tincidunt urna.<br />\r\n	.</p>\r\n', '', '', ''),
(4, 'Properties', NULL, 'properties here', NULL, NULL, NULL),
(5, 'Privacy Policy', NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempor, felis a commodo porttitor, ante nulla bibendum felis, vitae cursus nisi velit nec quam. In dolor nunc, vulputate non facilisis quis, pretium tincidunt urna. <br /><br />Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis blandit placerat est sed aliquam. Donec elementum metus ut lorem pharetra vitae pulvinar arcu dictum. Morbi quis nulla at eros semper consectetur sit amet sed neque. Etiam feugiat rutrum odio vel tincidunt.\n<br /><br />Aliquam lacinia arcu ac mi vestibulum fermentum non sit amet eros. Nulla in dolor magna. Nam quis urna non leo volutpat fringilla et sit amet dui. Mauris consectetur pulvinar tellus ut mollis.<br />\n\nSed non cursus est. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas mollis malesuada vehicula. Proin fermentum erat sit amet lorem feugiat varius euismod dolor venenatis. <br /><br />Praesent vehicula sapien at tortor feugiat quis iaculis est varius. Ut euismod faucibus purus, vitae gravida eros egestas facilisis. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum eu dolor non mi pretium varius ut in felis. Duis nec velit elit, in tempus turpis. Quisque quis lorem quis eros condimentum gravida at in ligula.<br /><br />\n\nPraesent tristique ultricies elit, ultricies blandit nisl hendrerit et. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tempor tincidunt odio, nec facilisis ipsum consequat quis. Nunc eget mi non neque placerat commodo volutpat ut metus. Vestibulum ac rutrum ligula. Suspendisse sit amet sapien lectus. Mauris a massa sit amet neque molestie pulvinar et non urna. <br /><br />Vivamus felis eros, tristique vel condimentum eu, molestie a velit. Suspendisse potenti. Sed feugiat ante id turpis iaculis condimentum. <br /><br />Etiam aliquam ante a mauris sagittis ultrices. Sed turpis mauris, bibendum eu auctor egestas, feugiat ac sem.', NULL, NULL, NULL),
(7, 'Consulting', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempor, felis a commodo porttitor, ante nulla bibendum felis, vitae cursus nisi velit nec quam. In dolor nunc, vulputate non facilisis quis, pretium tincidunt urna. <br /><br />Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis blandit placerat est sed aliquam. Donec elementum metus ut lorem pharetra vitae pulvinar arcu dictum. Morbi quis nulla at eros semper consectetur sit amet sed neque. Etiam feugiat rutrum odio vel tincidunt.\n<br /><br />Aliquam lacinia arcu ac mi vestibulum fermentum non sit amet eros. Nulla in dolor magna. Nam quis urna non leo volutpat fringilla et sit amet dui. Mauris consectetur pulvinar tellus ut mollis.<br />\n\nSed non cursus est. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas mollis malesuada vehicula. Proin fermentum erat sit amet lorem feugiat varius euismod dolor venenatis. <br /><br />Praesent vehicula sapien at tortor feugiat quis iaculis est varius. Ut euismod faucibus purus, vitae gravida eros egestas facilisis. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum eu dolor non mi pretium varius ut in felis. Duis nec velit elit, in tempus turpis. Quisque quis lorem quis eros condimentum gravida at in ligula.<br /><br />\n\nPraesent tristique ultricies elit, ultricies blandit nisl hendrerit et. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tempor tincidunt odio, nec facilisis ipsum consequat quis. Nunc eget mi non neque placerat commodo volutpat ut metus. Vestibulum ac rutrum ligula. Suspendisse sit amet sapien lectus. Mauris a massa sit amet neque molestie pulvinar et non urna. <br /><br />Vivamus felis eros, tristique vel condimentum eu, molestie a velit. Suspendisse potenti. Sed feugiat ante id turpis iaculis condimentum. <br /><br />Etiam aliquam ante a mauris sagittis ultrices. Sed turpis mauris, bibendum eu auctor egestas, feugiat ac sem.', NULL, NULL, NULL),
(8, 'Services', NULL, 'serviecs\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempor, felis a commodo porttitor,', NULL, NULL, NULL),
(9, 'Contact Us', NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempor, felis a commodo porttitor, ante nulla bibendum felis, vitae cursus nisi velit nec quam. In dolor nunc, vulputate non facilisis quis, pretium tincidunt urna. <br /><br />Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis blandit placerat est sed aliquam. Donec elementum metus ut lorem pharetra vitae pulvinar arcu dictum. Morbi quis nulla at eros semper consectetur sit amet sed neque. Etiam feugiat rutrum odio vel tincidunt.\n<br /><br />Aliquam lacinia arcu ac mi vestibulum fermentum non sit amet eros. Nulla in dolor magna. Nam quis urna non leo volutpat fringilla et sit amet dui. Mauris consectetur pulvinar tellus ut mollis.<br />\n\nSed non cursus est. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas mollis malesuada vehicula. Proin fermentum erat sit amet lorem feugiat varius euismod dolor venenatis. <br /><br />Praesent vehicula sapien at tortor feugiat quis iaculis est varius. Ut euismod faucibus purus, vitae gravida eros egestas facilisis. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum eu dolor non mi pretium varius ut in felis. Duis nec velit elit, in tempus turpis. Quisque quis lorem quis eros condimentum gravida at in ligula.<br /><br />\n\nPraesent tristique ultricies elit, ultricies blandit nisl hendrerit et. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tempor tincidunt odio, nec facilisis ipsum consequat quis. Nunc eget mi non neque placerat commodo volutpat ut metus. Vestibulum ac rutrum ligula. Suspendisse sit amet sapien lectus. Mauris a massa sit amet neque molestie pulvinar et non urna. <br /><br />Vivamus felis eros, tristique vel condimentum eu, molestie a velit. Suspendisse potenti. Sed feugiat ante id turpis iaculis condimentum. <br /><br />Etiam aliquam ante a mauris sagittis ultrices. Sed turpis mauris, bibendum eu auctor egestas, feugiat ac sem.', NULL, NULL, NULL),
(10, 'News', NULL, 'news here .....\r\nmmmmmm', NULL, NULL, NULL),
(12, 'Term & Condition', NULL, 'term and condition\r\n\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempor, felis a commodo porttitor, ante nulla bibendum felis, vitae cursus nisi velit nec quam. In dolor nunc, vulputate non facilisis quis, pretium tincidunt urna. <br /><br />Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis blandit placerat est sed aliquam. Donec elementum metus ut lorem pharetra vitae pulvinar arcu dictum. Morbi quis nulla at eros semper consectetur sit amet sed neque. Etiam feugiat rutrum odio vel tincidunt.\r\n<br /><br />Aliquam lacinia arcu ac mi vestibulum fermentum non sit amet eros. Nulla in dolor magna. Nam quis urna non leo volutpat fringilla et sit amet dui. Mauris consectetur pulvinar tellus ut mollis.<br />\r\n', NULL, NULL, NULL),
(13, 'Support', NULL, 'support\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempor, felis a commodo porttitor, ante nulla bibendum felis, vitae cursus nisi velit nec quam. In dolor nunc, vulputate non facilisis quis, pretium tincidunt urna. <br /><br \r\n\r\n', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(255) NOT NULL auto_increment,
  `Name` varchar(999) collate latin1_general_ci default NULL,
  `Email` varchar(999) collate latin1_general_ci default NULL,
  `sub_date` varchar(99) collate latin1_general_ci default NULL,
  `status` varchar(99) collate latin1_general_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `Name`, `Email`, `sub_date`, `status`) VALUES
(1, 'khalique', 'khalique.ahmed3@hotmail.com', 'Jun 14, 2012', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE `sub_category` (
  `id` int(11) NOT NULL auto_increment,
  `cat_id` varchar(99) collate latin1_general_ci default NULL,
  `title` varchar(99) collate latin1_general_ci default NULL,
  `title_clean` varchar(250) collate latin1_general_ci default NULL,
  `img1` varchar(99) collate latin1_general_ci default NULL,
  `s_img1` varchar(99) collate latin1_general_ci default NULL,
  `meta_title` varchar(250) collate latin1_general_ci default NULL,
  `meta_des` text collate latin1_general_ci,
  `meta_keywords` text collate latin1_general_ci,
  `status` varchar(99) collate latin1_general_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`id`, `cat_id`, `title`, `title_clean`, `img1`, `s_img1`, `meta_title`, `meta_des`, `meta_keywords`, `status`) VALUES
(1, '1', 'Sub Category title here', 'sub_category_title_here', 'img_sub_category/larg_1.jpg', 'img_sub_category/thumb_1.jpg', NULL, NULL, NULL, 'Yes');
