-- phpMyAdmin SQL Dump
-- version 3.5.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 16, 2013 at 01:27 PM
-- Server version: 5.5.30-30.2
-- PHP Version: 5.3.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `funchill_leadsourcecomplete`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(50) DEFAULT NULL,
  `admin_password` varchar(50) DEFAULT NULL,
  `admin_email` varchar(50) DEFAULT NULL,
  `admin_last_login` varchar(255) DEFAULT NULL,
  `admin_ip` varchar(255) DEFAULT NULL,
  `status` varchar(3) DEFAULT NULL,
  `pdate` date DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_name`, `admin_password`, `admin_email`, `admin_last_login`, `admin_ip`, `status`, `pdate`) VALUES
(1, 'admin', 'admin', 'shaukatkhan@hotmail.com', '24 July 2013  1:20PM', '127.0.0.1', 'Yes', '2013-01-30'),
(2, 'shaukat', 'admin', 'shaukat.khan188@yahoo.com', '30 January 2013  6:43PM', '127.0.0.1', 'Yes', '2013-01-30');

-- --------------------------------------------------------

--
-- Table structure for table `exclusive_leads`
--

CREATE TABLE IF NOT EXISTS `exclusive_leads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  `des` text COLLATE latin1_general_ci NOT NULL,
  `img1` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  `s_img1` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  `status` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `exclusive_leads`
--

INSERT INTO `exclusive_leads` (`id`, `title`, `des`, `img1`, `s_img1`, `status`) VALUES
(1, 'DAVID', 'A perfect company to work with. Communication is excellent, quality of work is top notch. Company sent updates on a daily basis always available for questions and comments. Comprehension of project was excellent. Will work with  Technologies again without question! - See more\r\n', NULL, NULL, 'Yes'),
(2, 'MAICHLE', 'A perfect company to work with. Communication is excellent, quality of work is top notch. Company sent updates on a daily basis always available for questions and comments. Comprehension of project was excellent. Will work with  Technologies again without question! - See more\r\n', NULL, NULL, 'Yes'),
(3, 'JOHN', 'A perfect company to work with. Communication is excellent, quality of work is top notch. Company sent updates on a daily basis always available for questions and comments. Comprehension of project was excellent. Will work with  Technologies again without question! - See more', NULL, NULL, 'Yes'),
(4, 'JHONY', 'A perfect company to work with. Communication is excellent, quality of work is top notch. Company sent updates on a daily basis always available for questions and comments. Comprehension of project was excellent. Will work with  Technologies again without question! - See more\r\n', NULL, NULL, 'Yes'),
(5, 'SMITH', 'A perfect company to work with. Communication is excellent, quality of work is top notch. Company sent updates on a daily basis always available for questions and comments. Comprehension of project was excellent. Will work with  Technologies again without question! - See more\r\n', NULL, NULL, 'Yes'),
(6, 'MISSI', 'A perfect company to work with. Communication is excellent, quality of work is top notch. Company sent updates on a daily basis always available for questions and comments. Comprehension of project was excellent. Will work with  Technologies again without question! - See more', NULL, NULL, 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `gallary`
--

CREATE TABLE IF NOT EXISTS `gallary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `des` text NOT NULL,
  `img1` varchar(255) NOT NULL,
  `s_img1` varchar(255) NOT NULL,
  `org_img` varchar(255) NOT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `gallary`
--

INSERT INTO `gallary` (`id`, `cat_id`, `title`, `url`, `des`, `img1`, `s_img1`, `org_img`, `status`) VALUES
(1, 0, 'EXCLUSIVE LEADS', '', '<p>\r\n	Our Exclusive Leads are generated campaign specific and can be branded as needed. These leads will be captured and sent directly to the end user.</p>\r\n', 'gallary/larg_1.jpg', 'gallary/thumb_1.jpg', 'img_headers/orgsize_1pro-exclusive.jpg', 'Yes'),
(2, 0, 'LIVE LEADS', '', '<p>\r\n	The Live Lead model is one of the most innovative and successful lead generation methods. We identify live potential customers that are attracted to your specific products and or services. We then deliver only active buyers for your business through either a live transfer call or from a lead that is transferred directly to you as soon as it has been submitted.</p>\r\n', 'gallary/larg_2.jpg', 'gallary/thumb_2.jpg', 'gallary/orgsize_2pro-call.jpg', 'Yes'),
(3, 0, 'WARM TRANSFERS', '', '<p>\r\n	Warm transfers are transferred calls to your agents where the customers are already pre-qualified for your business. We will enroll only those calls that fit your business as a potential customer.</p>\r\n', 'gallary/larg_3.jpg', 'gallary/thumb_3.jpg', 'gallary/orgsize_3pro-lead.jpg', 'Yes'),
(4, 0, 'TARGETED DATA LISTS', '', '<p>\r\n	At Lead Source Complete, we offer targeted data lists for email and telemarketing campaigns. These lists would include raw data from various sources where a customer has indicated interest in a certain product or service. We then offer the customer a place to provide their information so that businesses can contact them about the products or services they are searching for. We have millions of recordsin our database that is updated daily.</p>\r\n', 'gallary/larg_4.jpg', 'gallary/thumb_4.jpg', 'gallary/orgsize_4pro-target.jpg', 'Yes'),
(5, 0, 'CALL CENTER VERIFIED LEADS', '', '<p>\r\n	Lead Source Complete offers you leads that are verified through one of our call centers. These are generated through generic search and then verified by one of our call centers to ensure all the data collected is correct, accurate and that the client is verified as eligible and ready to act. For more information on any of our products or if the product you are looking for is not described here please feel free to contact us. We are always willing to innovate and take on a challenge. We would love the opportunity to speak with you about how we can make your business more profitable!</p>\r\n', 'gallary/larg_5.jpg', 'gallary/thumb_5.jpg', 'gallary/orgsize_5pro-call.jpg', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `gallary_cat`
--

CREATE TABLE IF NOT EXISTS `gallary_cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `des` text NOT NULL,
  `img1` varchar(255) NOT NULL,
  `s_img1` varchar(255) NOT NULL,
  `org_img` varchar(255) NOT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `gallary_cat`
--

INSERT INTO `gallary_cat` (`id`, `title`, `url`, `des`, `img1`, `s_img1`, `org_img`, `status`) VALUES
(1, 'OUR PRODUCTS', '', '', '', '', '', 'Yes'),
(2, 'SHARED LEADS', '', '', '', '', '', 'Yes'),
(3, 'EXCLUSIVE LEADS', '', '', '', '', '', 'Yes'),
(4, 'LIVE LEADS', '', '', '', '', '', 'Yes'),
(5, 'WARM TRANSFERS', '', '', '', '', '', 'Yes'),
(6, 'TARGETED DATA LISTS', '', '', '', '', '', 'Yes'),
(7, 'CALL CENTER VERIFIED LEADS', '', '', '', '', '', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `header_img`
--

CREATE TABLE IF NOT EXISTS `header_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  `img1` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  `s_img1` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  `status` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `header_img`
--

INSERT INTO `header_img` (`id`, `title`, `img1`, `s_img1`, `status`) VALUES
(1, 'Banner 1', 'Banner_images/larg_1.jpg', 'Banner_images/thumb_1.jpg', 'Yes'),
(2, 'Banner 2', 'Banner_images/larg_2.jpg', 'Banner_images/thumb_2.jpg', 'Yes'),
(3, 'Banner 3', 'Banner_images/larg_3.jpg', 'Banner_images/thumb_3.jpg', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `live_leads`
--

CREATE TABLE IF NOT EXISTS `live_leads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  `des` text COLLATE latin1_general_ci NOT NULL,
  `img1` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  `s_img1` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  `status` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `shared_tab`
--

CREATE TABLE IF NOT EXISTS `shared_tab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  `des` text COLLATE latin1_general_ci NOT NULL,
  `img1` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  `s_img1` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  `status` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `shared_tab`
--

INSERT INTO `shared_tab` (`id`, `title`, `des`, `img1`, `s_img1`, `status`) VALUES
(1, 'Banner 1', '<p>\r\n	test</p>\r\n', 'shared_img/larg_1.png', 'shared_img/thumb_1.png', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `site_contents`
--

CREATE TABLE IF NOT EXISTS `site_contents` (
  `content_id` int(11) NOT NULL AUTO_INCREMENT,
  `pg_title` varchar(255) DEFAULT NULL,
  `sub_title` varchar(100) NOT NULL,
  `title1` varchar(99) DEFAULT NULL,
  `des` longtext,
  `desc` text,
  `sub_main_O` varchar(255) DEFAULT NULL,
  `sub_main_T` varchar(255) DEFAULT NULL,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_des` text,
  `meta_keywords` text,
  PRIMARY KEY (`content_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `site_contents`
--

INSERT INTO `site_contents` (`content_id`, `pg_title`, `sub_title`, `title1`, `des`, `desc`, `sub_main_O`, `sub_main_T`, `meta_title`, `meta_des`, `meta_keywords`) VALUES
(1, 'Home', 'Pre-Qualified, Low-Cost Leads', 'QUALIFIED LEADS', '<h2>\r\n	Secret to our success is your commitment</h2>\r\n<p>\r\n	We would like to let you know that we offer the best Lead Service in all of South Florida. We have over 35 years experience in the Lead Services Industry. Any type lead that your business may need, we can find them for you at competitive rates. We deal with over 400 different Brokers Nationwide. We offer both shared and exclusive leads. Live leads are also an option. Contact us today for a free consultation quote.</p>\r\n<p>\r\n	We would like to let you know that we offer the best Lead Service in all of South Florida. We have over 35 years experience in the Lead Services Industry. Any type lead that your business may need, we can find them for you at competitive rates. We deal with over 400 different Brokers Nationwide. We offer both shared and exclusive leads. Live leads are also an option. Contact us today for a free consultation quote.</p>\r\n', '', '', '', 'Home', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. passages, and more recently with desktop publishing ', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. '),
(2, 'ABOUT US', '', 'About Us', '<p>\r\n	Lead Source Complete is a pioneer in online lead generation and offers you the most comprehensive network of information to attract leads and increase conversion rates for your business. We help businesses in gaining customers through extensive research and we connect potential customers with their required product and services.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Lead Source Complete is an all-in-one lead generation firm targeting customers on the basis of their informed choices and products/services that match their requirements. We have expertise in a wide variety of verticals including health, medical, home services, legal, insurance, EDU, consumables and more.</p>\r\n<p>\r\n	We are known for our consistency and quality of work. We provide our customers with the needed volume of prospective customers for their businesses. Our portfolio includes some of top ranking businesses nationally and internationally who have benefited from our lead generation services. We offer quality leads with higher conversion rates enabling businesses to bring in more revenue and drop bottom line costs.</p>\r\n<p>\r\n	Our lead costs are very competitive.At Lead Source Complete, we are just a call away. You can call us now for a free consultation and quote.Try our services now !</p>\r\n', '', '', '', 'About Us', 'About Us', 'About Us'),
(3, 'Generation Methods', '', 'Generation Methods', '<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been t</p>\r\n', '', '', '', 'Generation Methods', 'Generation Methods', 'Generation Methods'),
(5, 'Amenities', '', NULL, '<p>\r\n	<span>Lorem Ipsum is simply dummy text of the printing</span> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>\r\n<p>\r\n	Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s</p>\r\n<p>\r\n	Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s</p>\r\n<p>\r\n	Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s</p>\r\n', '', '', '', 'Amenities', 'Amenities', 'Amenities'),
(7, 'Question', 'Question', '', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', '', '', 'Question', 'Question', 'Question'),
(33, ' Our Products', '', ' Our Products', '<p>\r\n	<strong>SHARED LEADS</strong></p>\r\n<p>\r\n	Shared leads are the most common leads generation model. With Shared leads we offer you productive and quality leads along with these leads send to other companies as well. These leads can be sold to 2-5 customers depending on the type of lead required. This method is the most affordable way to get good quality and inexpensive leads. Our customers can get large bulk of shared leads at the most reasonable price.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>EXCLUSIVE LEADS</strong></p>\r\n<p>\r\n	Our Exclusive Leads model would mean that the leads you will receive will be only sent to you and to no other customer. This means that you will be first and the only one to reach your potential customer and would not face the trouble of customer already been spoken to some other company offering similar product or service as yours. Exclusive leads are surely a little costly as compared to shared leads but at Lead Source Complete we offer you most competitive rates for exclusive leads.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>LIVE LEADS</strong></p>\r\n<p>\r\n	Live Lead model is one of the most innovative and successful lead generation methods. We identify live potential customers and attract them for your business. We work hands in hands with your team to make purchase deal successful.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; We deliver only active buyers for your business through extensive research and search through various platforms. We make sure to give you a successful live deal with a cost which is quite reasonable.</p>\r\n<p>\r\n	&nbsp;</p>\r\n', ' Our Products', ' Our Products', ' Our Products', ' Our Products', ' Our Products', ' Our Products'),
(34, 'How It Works', 'How It Works', 'How It Works', 'How It Works', 'How It Works', 'How It Works', 'How It Works', 'How It Works', 'How It Works', 'How It Works'),
(35, 'Why Use', 'Why Use', 'Why Use', 'Why Use', 'Why Use', 'Why Use', 'Why Use', 'Why Use', 'Why Use', 'Why Use'),
(36, 'Privacy Policy ', 'Privacy Policy ', 'Privacy Policy ', 'Privacy Policy ', 'Privacy Policy ', 'Privacy Policy ', 'Privacy Policy ', 'Privacy Policy ', 'Privacy Policy ', 'Privacy Policy '),
(37, 'Terms & conditions ', 'Terms & conditions ', 'Terms & conditions ', 'Terms & conditions ', 'Terms & conditions ', 'Terms & conditions ', 'Terms & conditions ', 'Terms & conditions ', 'Terms & conditions ', 'Terms & conditions ');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE IF NOT EXISTS `subscribers` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `Name` varchar(999) COLLATE latin1_general_ci DEFAULT NULL,
  `Email` varchar(999) COLLATE latin1_general_ci DEFAULT NULL,
  `sub_date` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  `status` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `targeted_data`
--

CREATE TABLE IF NOT EXISTS `targeted_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  `des` text COLLATE latin1_general_ci NOT NULL,
  `img1` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  `s_img1` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  `status` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `warm_transfers`
--

CREATE TABLE IF NOT EXISTS `warm_transfers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  `des` text COLLATE latin1_general_ci NOT NULL,
  `img1` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  `s_img1` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  `status` varchar(99) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
