<?php include_once("include/config.inc.php");
	  include_once("include/__functions.php"); ?>

<html>
<head>
<meta name="description" content="<?php echo getInfo("site_contents","meta_des","WHERE `content_id`=1");?>">
<meta name="keywords" content="<?php echo getInfo("site_contents","meta_keywords","WHERE `content_id`=1");?>">
<title><?php echo getInfo("site_contents","meta_title","WHERE `content_id`=1");?></title>
<link rel="stylesheet" href="style.css" type="text/css">
<link rel="stylesheet" href="ie-7.0.css" type="text/css">
<link href="reset.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" type="text/css" href="engine1/style.css" />
<script type="text/javascript" src="engine1/jquery.js"></script>

<script type="text/javascript">
function validation(){

 
 var emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
  if(document.vform.name.value==''){
   alert('Please Enter Your First Name!');
   document.vform.name.focus();
   return false;
    }
	
	if(document.vform.lname.value==''){
   alert('Please Enter Your Last Name!');
   document.vform.lname.focus();
   return false;
    }
  if(document.vform.phone.value==''){
   alert('Please Enter Your Mobile / Phone!');
   document.vform.phone.focus();
   return false;
    }
   if(!(document.vform.email.value.match(emailRegex)) ){
   alert('Please Enter Valid Email!');
    document.vform.email.focus()
    return false;
   }
   
   if(document.vform.re_name.value==''){
   alert('Please Enter Re Name!');
   document.vform.re_name.focus();
   return false;
    }
	
	if(document.vform.web_name.value==''){
   alert('Please Enter website Name');
   document.vform.web_name.focus();
   return false;
    }
	
	if(document.vform.b_time.value==''){
   alert('Please Enter Best Time to Contact');
   document.vform.b_time.focus();
   return false;
    }
	
	if(document.vform.zip_code.value==''){
   alert('Please Enter zip code');
   document.vform.zip_code.focus();
   return false;
    }
	
	if(document.vform.target_name.value==''){
   alert('Please Enter Target State');
   document.vform.target_name.focus();
   return false;
    }
	
	if(document.vform.Primary.value==''){
   alert('Please Enter Primary Pratice Area');
   document.vform.Primary.focus();
   return false;
    }
	
	
	if(document.vform.Countries_name.value==''){
   alert('Please Enter Other Target State/Countries');
   document.vform.Countries_name.focus();
   return false;
    }
	
	if(document.vform.Areas_name.value==''){
   alert('Please Enter Other Target Practice Areas');
   document.vform.Areas_name.focus();
   return false;
    }
	
	
	document.vform.action='<?=$_SERVER['PHP_SELF'];?>';
    document.vform.method='POST';
	document.vform.submit();
	
}
</script>

</head>
<body style="background:url(images/bodybg.png);margin:auto;">
<div class="main">
  <div id="header">
    <div id="header-content">
      <div id="logo"> <a href="index.php"><img src="images/logo.jpg" alt="Lead source Complete"> </a></div>
      <div id="top-links">
        <div class="top-social-links "> <img src="images/leadsourcecomplete3_12.png" width="14" height="10" alt="email">
          <p>Contact Us</p>
        </div>
        <div class="top-social-links mgr"> <img src="images/leadsourcecomplete3_09.png" width="13" height="13" alt="email">
          <p>754 800-1831</p>
        </div>
        <div class="top-social-links mgr"> <img src="images/leadsourcecomplete3_06.png" width="15" height="14" alt="email">
          <p>Login</p>
        </div>
        <div class="top-social-links2 "> <a href="#"><img src="images/leadsourcecomplete3_03.png" width="92" height="31" alt=""></a> </div>
      </div>
      <div class="clear"></div>
      <?php include_once("header.php");?>
    </div>
  </div>
  <div class="clear"></div>
  <div class="main-warp">
    <div class="slider">
      <div id="wowslider-container1">
        <div class="ws_images">
          <ul>
          	<?php 
			$banner_sql = mysql_query("SELECT * FROM `header_img` WHERE status='Yes'");
			while($banner_rs = mysql_fetch_assoc($banner_sql)){
			?>
            <li><img src="<?php echo $banner_rs['img1']; ?>" alt="pic1" title="" id="wows1_<?php echo $banner_rs['id']; ?>"/></li>
            <!--<li><img src="data1/images/pic2.jpg" alt="pic2" title="" id="wows1_1"/></li>
            <li><img src="data1/images/pic3.jpg" alt="pic3" title="" id="wows1_2"/></li>-->
          	<?php } ?>
          </ul>
        </div>
        <div class="ws_bullets">
          <div> 
          <?php 
			$banner_sqll = mysql_query("SELECT * FROM `header_img` WHERE status='Yes'");
			while($banner_rss = mysql_fetch_assoc($banner_sqll)){
			?>
          <a href="#" title=""><img src="<?php echo $banner_rss['s_img1']; ?>" alt="pic3"/>3</a> 
          <?php } ?>
          </div>
        </div>
      </div>
      <script type="text/javascript" src="engine1/wowslider.js"></script>
      <script type="text/javascript" src="engine1/script.js"></script>
    </div>
    <div class="slider-text">
      <h1>Create Powerful Customer Connections</h1>
      <p>Find out how our focus on building, implementing, and supporting an easy-to-use, complete cloud <br/>
        contact center solution lets you create better customer interactions.</p>
      <br/>
      <a href="#" class="anchor-mrg"><img src="images/download.png" alt=""></a><a href="#" class="anchor-mrg2"><img src="images/learnmore.png" alt=""></a> </div>
    <div class="clear"></div>
    <div class="pointer">
      <div class="pointr-div"> <img src="images/pointer.png" width="34" height="20"> </div>
    </div>
    <div class="clear"></div>
    <div class="video-content">
      <div class="video-area"> </div>
      <div class="video-content-txt">
        <h1>Our leadsourcecomplete Contact Center</h1>
        <h2>Take A Quick Tour</h2>
        <p>This overview presents the  compelling strengths, as well as how the suite of cloud contact center solutions takes advantage of them to lower costs while improving agent productivity, customer satisfaction, and business agility.</p>
        <div class="link-btn2"><a href="javascript:;"><img src="images/view-demo.png" alt=""></a></div>
      </div>
    </div>
    <div class="clear"></div>
    <div class="content-text">
      <div class="left-content">
        <div class="left-box">
          <h1><?php echo getInfo("site_contents","sub_title","WHERE `content_id`=1");?> 
          <span><?php echo getInfo("site_contents","title1","WHERE `content_id`=1");?></span></h1>
          <?php echo getInfo("site_contents","des","WHERE `content_id`=1");?>
          <div class="link-btn2"><a href="javascript:;"><img src="images/view-demo.png" alt=""></a></div>
        </div>
        <div class="left-box" style="margin-top:20px;">
          <h1>QUALIFIED LEADS <span>Pre-Qualified, Low-Cost Leads</span></h1>
          <h2>Secret to our success is your commitment </h2>
          <p>We would like to let you know that we offer the best Lead Service in all of South Florida. We have over 35 years experience in the Lead Services Industry. Any type lead that your business may need, we can find them for you at competitive rates. We deal with over 400 different Brokers Nationwide. We offer both shared and exclusive leads. Live leads are also an option. Contact us today for a free consultation quote.</p>
          <p>We would like to let you know that we offer the best Lead Service in all of South Florida. We have over 35 years experience in the Lead Services Industry. Any type lead that your business may need, we can find them for you at competitive rates. We deal with over 400 different Brokers Nationwide. We offer both shared and exclusive leads. Live leads are also an option. Contact us today for a free consultation quote.</p>
          <div class="link-btn2"><a href="javascript:;"><img src="images/view-demo.png" alt=""></a></div>
        </div>
      </div>
      <div class="right-content">
        <div class="form-area">
        <?php
   		if ($_SERVER['REQUEST_METHOD'] != 'POST'){
      	$self = $_SERVER['PHP_SELF'];
		?>
          <form name="vform" id="third" >
            <table style="width:359px;margin:auto;margin-left:10px;">
              <tr class="tr">
              <tr>
                <td class="label td">First Name</td>
                <td class="label td">Last Name</td>
              </tr>
              <tr>
                <td class="td"><input type="text" name="name" id="full_name"></td>
                <td class="td"><input type="text" name="lname" id="lname"></td>
              </tr>
              </tr>
              
              <tr class="tr">
              <tr>
                <td class="label td">Phone</td>
                <td class="label td">Primary Email Address</td>
              </tr>
              <tr>
                <td class="td"><input type="text" name="phone" id="phone"></td>
                <td class="td"><input type="text" name="email" id="email"></td>
              </tr>
              </tr>
              
              <tr class="tr">
              <tr>
                <td class="label td"> Name</td>
                <td class="label td">Website</td>
              </tr>
              <tr>
                <td class="td"><input type="text" name="re_name" id="re_name"></td>
                <td class="td"><input type="text" name="web_name" id="web_name"></td>
              </tr>
              </tr>
              
              <tr class="tr">
              <tr>
                <td class="label td">Best Time to Contact</td>
                <td class="label td">Zip Code</td>
              </tr>
              <tr>
                <td class="td"><input type="text" name="b_time" id="b_time"></td>
                <td class="td"><input type="text" name="zip_code" id="zip_code"></td>
              </tr>
              </tr>
              
              <tr class="tr">
              <tr>
                <td class="label td">Target State</td>
                <td class="label td">Primary Pratice Area</td>
              </tr>
              <tr>
                <td class="td"><input type="text" name="target_name" id="target_name"></td>
                <td class="td"><input type="text" name="Primary" id="Primary"></td>
              </tr>
              </tr>
              
              <tr class="tr">
              <tr >
                <td class="label td">Other Target State/Countries</td>
                <td class="label td">Other Target Practice Areas</td>
              </tr>
              <tr>
                <td class="td"><input type="text" name="Countries_name" id="Countries_name"></td>
                <td class="td"><input type="text" name="Areas_name" id="Areas_name"></td>
              </tr>
              </tr>
              
              <tr class="" style="width:100%;background:red;">
                <table style="width:100%;margin-left:10px;margin-top:10px;margin-bottom:10px;">
                  <tr>
                    <td class="label" > Nationwide,&nbsp;&nbsp;Statewide&nbsp;&nbsp; or Regional Coverage? </td>
                  </tr>
                </table>
              </tr>
              <tr class="td">
                <table style="width:100%;margin-left:10px;margin-top:10px;margin-bottom:10px;">
                  <tr>
                     <td class="label td2"><input type="radio" name="radio"  />Nationwide</td>
                     <td class="label td2"><input type="radio" name="radio"  />statewide</td>
                     <td class="label td2"><input type="radio" name="radio"  />Regione</td>
                   </table>
              </tr>
            </table>
            <br/>
            <br/>
            <a href="javascript:void(0);" onClick="validation();" style="margin-top:30px;margin-left:50px;"><img src="images/chek.png"></a>
          </form>
        
        
        
         <?php
		} 
		else 
		{
    	error_reporting(0);


		$to = 'adnan.mazaarzai@gmail.com';
		$name = stripslashes($_POST['name']); //sender's name
		$lname = stripslashes($_POST['lname']); //sender's email
		$phone = stripslashes($_POST['phone']); //sender's email
		$email = stripslashes($_POST['email']); //sender's email
		$re_name = stripslashes($_POST['re_name']); //sender's email
		$web_name = stripslashes($_POST['web_name']); //sender's email
		$b_time = stripslashes($_POST['b_time']); //sender's email
		$zip_code = stripslashes($_POST['zip_code']); //sender's email
		$target_name = stripslashes($_POST['target_name']); //sender's email
		$Primary = stripslashes($_POST['Primary']); //sender's email
		$Countries_name = stripslashes($_POST['Countries_name']); //sender's email
		$Areas_name = stripslashes($_POST['Areas_name']); //sender's email
		//$email = stripslashes($_POST['email']); //sender's email

//The subject
$subject  = "Lead Source Complete"; //The default subject. Will appear by default in all messages. Change this if you want.
$subject .= stripslashes($_POST['subject']); // the subject

	$headers = "From: " . $name . "\r\n";
	$headers .= "Content-type: text/html\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$img_url="http://funchill.com/demo/leadsourcecomplete/images/logo.png";
	$msg .="<img src='".$img_url."' />";
	$msg .= "<h1>User information </h1><br/><br/>"; //Title
	$msg  = "From : $name &nbsp; $lname <br/><br/>";  //add sender's name to the message
	$msg .= "E-mail : $email <br/><br/>";  //add sender's email to the message
	$msg .= "Subject : $subject <br/><br/>";
	$msg .= "Phone / Mobile : $phone <br/><br/>"; 
	$msg .= "Name : $re_name <br/><br/>";
	$msg .= "Website : $web_name <br/><br/>";
	$msg .= "Best Time to Contact : $b_time <br/><br/>";
	$msg .= "Zip Code : $zip_code <br/><br/>";
	$msg .= "Target State : $target_name <br/><br/>";
	$msg .= "Primary Pratice Area : $Primary <br/><br/>";
	$msg .= "Other Target State/Countries : $Countries_name <br/><br/>";
	$msg .= "Other Target Practice Areas : $Areas_name <br/><br/>";
	
	//$msg .= "Message : \r\n".stripslashes($_POST['message'])."<br/><br/><br/><br/>";  //the message itself
	
	
	
	$msg .= "User IP : ".$_SERVER["REMOTE_ADDR"]."<br/><br/><br/>"; //Sender's IP
	$msg .= "Browser info : ".$_SERVER["HTTP_USER_AGENT"]."<br/><br/><br/>"; //User agent
	$msg .= "User come from : ".$_SERVER["HTTP_REFERER"]."<br/><br/><br/>"; //Referrer
// END Extras



      	$response=mail($to, $subject, $msg, $headers);

      	echo nl2br("
	   	<div>
			<h1 align='center' style='font-family: Myriad Pro; font-size: 31px; font-weight: Regular; color: #1295d1; margin-top: 50px;'>Congratulations !!</h1>
			<h1 align='center' style='color: #fff;
text-decoration: none;
font-size: 14px; font-family: Myriad Pro;' >Thank you <b>".$name."</b>, your message is sent......!<br /> </h1>
		</div>
	   ");

	}


?>
        
        
        </div>
      </div>
    </div>
    <div class="clear"></div>
    <?php include_once("Links_page.php");?>
  </div>
  <div class="clear"></div>
  
  <?php include_once("footer.php");?>

</div>
</body>
</html>
